from datetime import datetime
from utils.general import download_files, extract_files, csv_generation, import_csv_to_database,get_logger
import os

logger = get_logger()

# FIXED VARIABLES
user = 'ossuser'
password = 'Changeme_123'
directory = '/export/home/sysm/opt/oss/server/var/fileint/cm/GExport'

# DB USER PASS
db_user = 'lrgsouza'
db_password = 'luc4S2020'

# MUTABLE VARIABLES
hosts = ['201.69.136.103', '201.69.136.104', '201.69.136.105', '201.69.136.106',
         '201.69.136.107', '201.69.136.108', '201.69.136.109', '201.69.136.110']

# define zip dir to ftp function with normal bar /
basepath = os.path.dirname(__file__)
directory_path = os.path.abspath(os.path.join(basepath, "media"))
zip_directory = os.path.abspath(os.path.join(directory_path, 'ZIP'))
zip_directory = zip_directory.replace("\\","/")
Mydir = zip_directory + "/"

# EST FLAGS
dld = True
extract = True
csvgen = True
dbimport = True

# FULL PROCESS
started = datetime.now()
try:
	if dld:
		download_files(hosts, user, password, directory, Mydir)
	if extract:
		extract_files()
	if csvgen:
		csv_generation()
	if dbimport:
		import_csv_to_database(db_user,db_password)

	finished = datetime.now()

	print(f'Started: {started}\n'
		  f'Finished: {finished}\n'
		  f'Delta: {finished - started}')
except Exception as e:
	logger.warning(f'Erro não mapeado!')
	logger.error(e, exc_info=True)

