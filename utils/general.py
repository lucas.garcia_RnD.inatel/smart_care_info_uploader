import ftplib
import pandas as pd
import re
import os
from os import listdir
from os.path import isfile, join
import gzip
import shutil
from xml.dom import minidom
from datetime import datetime
import mysql.connector as mysql
from sqlalchemy import create_engine
import math
import platform

import logging


def get_logger():
    #====================logger
    basepath = os.path.dirname(__file__)
    directory_path = os.path.abspath(os.path.join(basepath, "..", "logger"))
    logger_file = os.path.abspath(os.path.join(directory_path, 'std.log'))
    # now we will Create and configure logger
    logging.basicConfig(filename=logger_file,
                        format='%(asctime)s %(message)s',
                        filemode='a')
    # Let us Create an object
    logger = logging.getLogger()
    # Now we are going to Set the threshold of logger to DEBUG
    #logger.setLevel(logging.)
    return logger


def roundup(x):
    return int(math.ceil(x / 100.0)) * 100


def get_date_info(row):
    return row['file_name'].split('_')[-1].split('.')[0]


def get_literal_name(row):
    return '_'.join(row['file_name'].split('_')[:-1])


def ftp_connection(host, user, password, directory):
    ftp = ftplib.FTP(host)
    ftp.login(user, password)
    ftp.cwd(directory)
    files = ftp.nlst()
    file_names = []
    file_directories = []
    for current_file in files:
        if '.' in current_file:
            file_names.append(current_file)
            current_directory = f'{directory}/{current_file}'
            file_directories.append(current_directory)
        else:
            subdirectory = f'{directory}/{current_file}'
            ftp.cwd(subdirectory)
            sub_files = ftp.nlst()
            for current_sub_file in sub_files:
                file_names.append(current_sub_file)
                current_directory = f'{subdirectory}/{current_sub_file}'
                file_directories.append(current_directory)

    df_ftp = pd.DataFrame(columns=['file_name', 'file_directory'])
    df_ftp['file_name'] = file_names
    df_ftp['file_directory'] = file_directories

    return ftp, df_ftp


def download_files(hosts, user, password, directory, Mydir):
    logger = get_logger()
    print('>>>>Starting files download!<<<<')

    for host in hosts:

        print(f'>Downloading {host} files!<')
        logger.warning(f'>Downloading {host} files!<')

        ftp, df_ftp = ftp_connection(host, user, password, directory)

        # ALL GEXPORT ON NAME

        df_download = df_ftp[df_ftp['file_name'].str.contains('GEXPORT', flags=re.IGNORECASE)].copy()
        if df_download.empty:
            continue
        #filtrando tudo que ~ NÃO CONTEM _INC_
        df_download = df_download[~df_download['file_name'].str.contains('_INC_', flags=re.IGNORECASE)].copy()
        if df_download.empty:
            continue

        df_download['date'] = df_download.apply(lambda row: get_date_info(row), axis=1)
        df_download['literal_name'] = df_download.apply(lambda row: get_literal_name(row), axis=1)
        df_download.sort_values(by='date', inplace=True, ascending=False)
        df_download.drop_duplicates(subset='literal_name', inplace=True)

        for index, row in df_download.iterrows():
            file_to_download = row['file_directory']
            file_to_download_name = row['file_name']
            ftp.retrbinary("RETR " + file_to_download, open(
                Mydir + file_to_download_name,
                'wb').write)
            print(f'Downloaded: {file_to_download_name}')
            logger.warning(f'Downloaded: {file_to_download_name}')

        ftp.close()

    print('>>>>Finished files download!<<<<')
    logger.warning('>>>>Finished files download!<<<<')


def extract_files():
    logger = get_logger()
    basepath = os.path.dirname(__file__)
    directory_path = os.path.abspath(os.path.join(basepath, "..", "media"))
    zip_directory = os.path.abspath(os.path.join(directory_path, 'ZIP'))
    xml_directory = os.path.abspath(os.path.join(directory_path, 'XML'))

    # zip_directory = f'..\media\ZIP'
    # xml_directory = f'..\media\XML'

    zip_files = [f for f in listdir(zip_directory) if isfile(join(zip_directory, f))]

    print('>>>>Starting files extraction!<<<<')
    logger.warning('>>>>Starting files extraction!<<<<')

    for extracting in zip_files:
        full_zip_dir = f'{zip_directory}/{extracting}'
        full_target_dir = f'{xml_directory}/{extracting[:-3]}'
        with gzip.open(full_zip_dir, 'rb') as f_in:
            with open(full_target_dir, 'wb') as f_out:
                shutil.copyfileobj(f_in, f_out)

        os.remove(full_zip_dir)

    logger.warning('>>>>Finished files extraction!<<<<')
    print('>>>>Finished files extraction!<<<<')


def csv_generation():
    logger = get_logger()
    basepath = os.path.dirname(__file__)
    directory_path = os.path.abspath(os.path.join(basepath, "..", "media"))
    xml_directory = os.path.abspath(os.path.join(directory_path, 'XML'))
    csv_directory = os.path.abspath(os.path.join(directory_path, 'CSV'))
    bkp_directory = os.path.abspath(os.path.join(directory_path, 'BKP'))
    print('>>>>Starting CSV generation!<<<<')
    logger.warning('>>>>Starting CSV generation!<<<<')

    #LIMPANDO PASTA BKP
    BKP_files = [f for f in listdir(bkp_directory) if isfile(join(bkp_directory, f))]
    for file in BKP_files:
        file_to_delete = os.path.join(f'{bkp_directory}/', file)
        os.remove(file_to_delete)

    # xml_directory = f'..\\media\\XML'
    xml_files = [f for f in listdir(xml_directory) if isfile(join(xml_directory, f))]

    #deletando CSVs antigos
    ne_type_folders = [f for f in os.listdir(f'{csv_directory}/')]
    for ne_type in ne_type_folders:
        current_csv = [f for f in os.listdir(f'{csv_directory}/{ne_type}')]
        if current_csv:
            logger.warning(f"Start Cleaning {ne_type} folder")
            print(f"Start Cleaning {ne_type} folder")
            for cur_csv in current_csv:
                path_to_delete = os.path.join(f'{csv_directory}/{ne_type}', cur_csv)
                path_to_delete = path_to_delete.replace('\\', '/')
                os.remove(path_to_delete)
            logger.warning(f"Finish Cleaning {ne_type} folder")
            print(f"Finish Cleaning {ne_type} folder")

    for file_name in xml_files:
        try:
            if not "GEXPORT" in file_name.upper():
                continue
            else:
                doc = minidom.parse(f'{xml_directory}/{file_name}')
                commands = doc.getElementsByTagName("class")

                ne_type = commands[0].getAttribute('name')
                ne_version = commands[0].getElementsByTagName("object")[0].getAttribute('version')
                ne_name_fragments = file_name.split('_')
                ne_name = f'{ne_name_fragments[1]}_{ne_name_fragments[2]}_{ne_name_fragments[3]}'
                now = datetime.now().date().strftime("%Y-%m-%d")

                # PULANDO TABELAS INDESEJÁVEIS
                exclude_types = ['eSight-NFV','MGW']
                
                if ne_type in exclude_types:
                    os.remove(f'{xml_directory}/{file_name}')
                    print(f'Unwanted type, do not process: {file_name}')
                    logger.warning(f'Unwanted type, do not process: {file_name}')
                    continue

                if not os.path.isdir(f'{csv_directory}/{ne_type}'):
                    os.makedirs(f'{csv_directory}/{ne_type}')

                commands = commands[1:]
                full_df_dict = {}
                done_commands = []
                for current_command in commands:
                    if not current_command.getElementsByTagName("object"):
                        continue
                    else:
                        objects = current_command.getElementsByTagName("object")
                        df_dicts_list = []
                        for object in objects:
                            df_dicts = {'NE_NAME': ne_name,
                                        'NE_VERSION': ne_version,
                                        'DATE': now}
                            parameters = object.getElementsByTagName('parameter')
                            for parameter in parameters:
                                df_dicts[f'c_{parameter.getAttribute("name")}'] = f'{parameter.getAttribute("value")}'
                            df_dicts_list.append(df_dicts)
                    if not current_command.getAttribute("name") in done_commands:
                        full_df_dict[current_command.getAttribute("name")] = df_dicts_list
                        done_commands.append(current_command.getAttribute("name"))
                    else:
                        full_df_dict[current_command.getAttribute("name")].extend(df_dicts_list)

                for current_command in done_commands:
                    literal_command = current_command.split('_')[-2]
                    df = pd.DataFrame(full_df_dict[current_command])
                    if not os.path.isfile(f'{csv_directory}/{ne_type}/{literal_command}.csv'):
                        df.to_csv(f'{csv_directory}/{ne_type}/{literal_command}.csv', index=False)
                    else:
                        df_csv = pd.read_csv(f'{csv_directory}/{ne_type}/{literal_command}.csv')
                        df_final = pd.concat([df_csv, df], axis=0, ignore_index=0)
                        os.remove(f'{csv_directory}/{ne_type}/{literal_command}.csv')
                        df_final.to_csv(f'{csv_directory}/{ne_type}/{literal_command}.csv', index=False)

                os.remove(f'{xml_directory}/{file_name}')
                print(f'Done: {file_name}')
                logger.warning(f'Done: {file_name}')
        except Exception as e:
            logger.error(e, exc_info=True)
            logger.warning(f'Error on: {file_name} --> moved to backup')
            #movendo para backup
            os.rename(f'{xml_directory}/{file_name}', f'{bkp_directory}/{file_name}')
            print(f'ERROR: {file_name} = {e}')

    print('>>>>Finished CSV generation!<<<<')


def import_csv_to_database(db_user,db_password):
    logger = get_logger()
    basepath = os.path.dirname(__file__)
    directory_path = os.path.abspath(os.path.join(basepath, "..", "media"))
    directory_csv = os.path.abspath(os.path.join(directory_path, 'CSV'))

    print('>>>>Starting Database importing!<<<<')
    logger.warning('>>>>Starting Database importing!<<<<')

    # directory_csv = f'..\\media\\CSV\\'
    ne_types = os.listdir(directory_csv)

    db = mysql.connect(
        host="172.29.200.126",
        user=db_user,
        passwd=db_password,
        allow_local_infile=True
    )
    cursor = db.cursor()

    for ne_type in ne_types:

        print(f'>Working on {ne_type} Database!<')
        logger.warning(f'>Working on {ne_type} Database!<')

        ne_type_dir = f'{directory_csv}/{ne_type}/'

        # CHECK IF DATABASE EXISTS
        neType = ne_type.replace('-', '')
        cursor.execute(f'CREATE DATABASE IF NOT EXISTS {neType};')
        db.commit()

        all_mo = os.listdir(ne_type_dir)

        for current_mo in all_mo:

            mo = current_mo.replace('.csv', '')

            try:
                # CHECK IF TABLE EXISTS

                current_mo_dir = f'{ne_type_dir}{current_mo}'

                df_csv = pd.read_csv(current_mo_dir)

                list_colums = list(df_csv.columns)

                now = df_csv['DATE'].iloc[0]

                sql_table_creation = f'CREATE TABLE IF NOT EXISTS {neType}.t_{mo} ('
                for column in list_colums:
                    if not column == 'DATE':
                        sql_insert_col = f"{column} varchar(100) DEFAULT '-', "
                        sql_table_creation = sql_table_creation + sql_insert_col
                    else:
                        sql_insert_col = f"{column} date DEFAULT '{now}', "
                        sql_table_creation = sql_table_creation + sql_insert_col

                sql_table_creation = sql_table_creation[:-2] + ');'

                cursor.execute(sql_table_creation)
                db.commit()

                # GET TABLE COLUMNS

                db_connection_str = f'mysql+pymysql://{db_user}:{db_password}@172.29.200.126'
                db_connection = create_engine(db_connection_str)
                df_table = pd.read_sql(f'SELECT * FROM {neType}.t_{mo} LIMIT 1', con=db_connection)
                table_columns = list(df_table.columns)

                add_columns = [i for i in list_colums if not i in table_columns]

                if add_columns:
                    for adding in add_columns:
                        sql_table_creation = f"ALTER TABLE {neType}.t_{mo} ADD {adding} varchar(100) DEFAULT '-'"
                        cursor.execute(sql_table_creation)
                        table_columns.append(adding)
                    db.commit()

                add_csv_columns = [i for i in table_columns if not i in list_colums]
                for column in add_csv_columns:
                    df_csv[column] = '-'

                df_csv = df_csv[table_columns]

                df_csv.fillna('-', inplace=True)

                os.remove(current_mo_dir)

                df_csv.to_csv(current_mo_dir, mode='a', header=False, index=False)

                upload_current_mo_dir = current_mo_dir.replace('\\', '/')

                # TEMPORARIO - DELETA ITENS A PARTIR DE 13-04
                # del_sql = f'delete FROM {neType}.t_{mo} where DATE > \'2022-04-12\';'
                # cursor.execute(del_sql)
                # db.commit()

                cur_platform = platform.system()

                if cur_platform.upper() == 'WINDOWS':
                    sql = f'LOAD DATA LOCAL INFILE "{upload_current_mo_dir}" REPLACE INTO TABLE {neType}.t_{mo}\n' \
                          f'FIELDS TERMINATED BY ","  ENCLOSED BY \'"\'\n' \
                          f'LINES TERMINATED BY \'\r\n\';'
                else:
                    sql = f'LOAD DATA LOCAL INFILE "{upload_current_mo_dir}" REPLACE INTO TABLE {neType}.t_{mo}\n' \
                          f'FIELDS TERMINATED BY ","  ENCLOSED BY \'"\'\n' \
                          f'LINES TERMINATED BY \'\n\';'

                cursor.execute(sql)
                db.commit()

                print(f'Done: {mo}')
                logger.warning(f'Done: {mo}')

                os.remove(current_mo_dir)

            except Exception as e:
                logger.warning(f'Error pickup on MO: {mo}')
                logger.error(e, exc_info=True)
                try:
                    if 'DATA TOO LONG FOR COLUMN' in e.msg.upper():
                        all_columns = list(df_csv.columns)
                        for check_col in all_columns:
                            df_col_len = int(df_csv[check_col].astype(str).str.encode(encoding='utf-8').str.len().max())
                            df_col_len = roundup(df_col_len)
                            if df_col_len > 100:
                                sql = f"ALTER TABLE {neType}.t_{mo} \n" \
                                      f"CHANGE COLUMN `{check_col}` `{check_col}`" \
                                      f" VARCHAR({df_col_len}) DEFAULT '-';"

                                cursor.execute(sql)
                        db.commit()


                        cur_platform = platform.system()
                        if cur_platform.upper() == 'WINDOWS':
                            sql = f'LOAD DATA LOCAL INFILE "{upload_current_mo_dir}" REPLACE INTO TABLE {neType}.t_{mo}\n' \
                              f'FIELDS TERMINATED BY ","  ENCLOSED BY \'"\'\n' \
                              f'LINES TERMINATED BY \'\r\n\';'
                        else:
                            sql = f'LOAD DATA LOCAL INFILE "{upload_current_mo_dir}" REPLACE INTO TABLE {neType}.t_{mo}\n' \
                                  f'FIELDS TERMINATED BY ","  ENCLOSED BY \'"\'\n' \
                                  f'LINES TERMINATED BY \'\n\';'

                        cursor.execute(sql)
                        db.commit()

                        print(f'Done: {mo}')
                        logger.warning(f'Done: {mo}')

                        os.remove(current_mo_dir)

                except Exception as e:
                    os.remove(current_mo_dir)
                    logger.error(e, exc_info=True)
                    print(f'ERROR: {mo} - file deleted --({e})--')
                    logger.warning(f'ERROR: {mo} - file deleted --({e})--')

    print('>>>>Finished Database importing!<<<<')
    logger.warning('>>>>Finished Database importing!<<<<')

